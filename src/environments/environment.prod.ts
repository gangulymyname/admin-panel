/*
  
*/
export const environment = {
  production: true,
  baseURL: 'https://api.ememe.tk/',
  mediaURL: 'https://api.ememe.tk/uploads/',
  onesignal: {
    appId: 'd9d6c621-8d9d-4a2f-9098-cfae13036d8b',
    googleProjectNumber: '888633870378',
    restKey: 'YmQ4OWYwNTEtZjlkMy00NzhhLTkyY2UtN2MwZWY0YjYyNTFm'
  },
  general: {
    symbol: '$',
    code: 'USD'
  },
  authToken: '123456789'
};
