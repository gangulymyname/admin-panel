import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { ActivatedRoute, Router } from '@angular/router';
import { ApisService } from './../../services/apis.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {
  stores: any[] = [];
  storeId: any;
  // modeInOut: any;

  products: any[] = [];
  dummProducts: any[] = [];
  dummy = Array(5);
  page = 1;
  modeInOut:any[] = [];
  stockQty:any[] = [];

  constructor(
    public api: ApisService,
    private route: ActivatedRoute,
    private toastyService: ToastyService,
    private spinner: NgxSpinnerService,
    private navCtrl: Location,
    private router: Router
  ) { 
    const param = {
      id: localStorage.getItem('uid')
    }
    this.api.auth(param).then((data) => {
      console.log('auth data->>', data);
      if (data !== true) {
        localStorage.removeItem('uid');
        this.router.navigate(['login']);
      }
    }, error => {
      console.log(error);
      localStorage.removeItem('uid');
      this.router.navigate(['login']);
    }).catch((error) => {
      console.log(error);
      localStorage.removeItem('uid');
      this.router.navigate(['login']);
    });
    this.getStore();
  }

  ngOnInit(): void {
  }

  error(message) {
    const toastOptions: ToastOptions = {
      title: this.api.translate('Error'),
      msg: this.api.translate(message),
      showClose: true,
      timeout: 2000,
      theme: 'default',
      onAdd: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been added!');
      },
      onRemove: () => {
        console.log('Toast  has been removed!');
      }
    };
    // Add see all possible types in one shot
    this.toastyService.error(toastOptions);
  }

  success(message) {
    const toastOptions: ToastOptions = {
      title: this.api.translate('Success'),
      msg: this.api.translate(message),
      showClose: true,
      timeout: 2000,
      theme: 'default',
      onAdd: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been added!');
      },
      onRemove: () => {
        console.log('Toast  has been removed!');
      }
    };
    // Add see all possible types in one shot
    this.toastyService.success(toastOptions);
  }

  getStore() {
    this.stores = [];
    this.api.get('stores').then((datas: any) => {
      console.log(datas);

      if (datas && datas.data.length) {
        this.stores = datas.data;
      }
    }, error => {
      console.log(error);
      this.error(this.api.translate('Something went wrong'));
    }).catch(error => {
      this.spinner.hide();
      console.log(error);
      this.error(this.api.translate('Something went wrong'));
    });
  }

  onChangeStore(storeId: any){
    this.storeId = storeId;
    const param = {
      storeId: this.storeId
    };
    this.spinner.show();
    this.api.post('stock/getByStoreId', param).then((data: any) => {
      this.spinner.hide();
      console.log('response', data);
      if (data && data.data && data.data.length) {
        this.products = data.data;
        this.dummProducts = data.data;
      }
    }, error => {
      this.spinner.hide();
      console.log('errror', error);
      this.error(this.api.translate('Something went wrong'));
    }).catch(error => {
      this.spinner.hide();
      console.log(error);
      this.error(this.api.translate('Something went wrong'));
    });
  }

  updateStock(product_id: any, index: any){
    console.log(this.modeInOut[index]);
    console.log(this.stockQty[index]);
    const param = {
      storeId: this.storeId,
      product_id: product_id,
      mode: this.modeInOut[index],
      stockQty: this.stockQty[index]
    };
    this.spinner.show();
    this.api.post('stock/updateStock', param).then((data: any) => {
      this.spinner.hide();
      console.log('response', data);
      if (data && data.data && data.data.length) {
        this.api.alerts(this.api.translate('Success'), this.api.translate('Stock updated'), 'success');
        //this.products = data.data;
      }
    }, error => {
      this.spinner.hide();
      console.log('errror', error);
      this.error(this.api.translate('Something went wrong'));
    }).catch(error => {
      this.spinner.hide();
      console.log(error);
      this.error(this.api.translate('Something went wrong'));
    });
  }

  search(string: any){}
}
