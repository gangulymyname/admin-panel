import { ToastyService } from 'ng2-toasty';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, NavigationExtras } from '@angular/router';
import { ApisService } from './../../services/apis.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.css']
})
export class PurchaseComponent implements OnInit {

  purchase: any[] = [];
  dummPurchase: any[] = [];
  dummy = Array(5);
  page = 1;

  constructor(
    public api: ApisService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private toastyService: ToastyService,
  ) {
    const param = {
      id: localStorage.getItem('uid')
    }
    this.api.auth(param).then((data) => {
      console.log('auth data->>', data);
      if (data !== true) {
        localStorage.removeItem('uid');
        this.router.navigate(['login']);
      }
    }, error => {
      console.log(error);
      localStorage.removeItem('uid');
      this.router.navigate(['login']);
    }).catch((error) => {
      console.log(error);
      localStorage.removeItem('uid');
      this.router.navigate(['login']);
    });
    this.getPurchase();
  }

  ngOnInit(): void {
  }

  getPurchase(){
    this.api.get('purchase').then((data: any) => {
      console.log('purchase', data);
      this.dummy = [];
      if (data && data.status === 200 && data.data && data.data.length > 0) {
        this.purchase = data.data;
        this.dummPurchase = data.data;
      }
    }).catch(error => {
      console.log(error);
    });
  }
  search(string) {
    this.resetChanges();
    console.log('string', string);
    this.purchase = this.filterItems(string);
  }
  protected resetChanges = () => {
    this.purchase = this.dummPurchase;
  }
  filterItems(searchTerm) {
    return this.purchase.filter((item) => {
      return item.purchase_no.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });

  }
  openOrder(item) {
    console.log(item);
    const navData: NavigationExtras = {
      queryParams: {
        id: item.id,
        edit: true
      }
    };
    this.router.navigate(['manage-purchase'], navData);
  }
  createNew(){this.router.navigate(['manage-purchase']);}
}
