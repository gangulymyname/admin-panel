import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagePurchaseRoutingModule } from './manage-purchase-routing.module';
import { ManagePurchaseComponent } from './manage-purchase.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ManagePurchaseComponent],
  imports: [
    CommonModule,
    ManagePurchaseRoutingModule,
    SharedModule
  ]
})
export class ManagePurchaseModule { }
