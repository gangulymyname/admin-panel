import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManagePurchaseComponent } from './manage-purchase.component';

const routes: Routes = [{ path: '', component: ManagePurchaseComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagePurchaseRoutingModule { }
